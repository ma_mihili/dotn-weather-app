﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace weather_app
{
    public partial class CountrySelectForm : Form
    {
        public string city;

        public CountrySelectForm(string text)
        {
            InitializeComponent();
            label1.Text = text;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.city = textBox1.Text;
            this.DialogResult = DialogResult.OK;
        }
    }
}

﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace weather_app
{
    public partial class Form1 : Form
    {
        public string city = "Zagreb";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.renderWeather();
        }

        private void renderWeather()
        {
            this.FetchCurrentWeather();
        }

        private void FetchCurrentWeather()
        {
            using (WebClient wc = new WebClient())
            {
                string url = "http://api.openweathermap.org/data/2.5/weather?q=" + this.city + "&appid=45241fc43340385f1741c0f12a8e2d82&units=metric";
                wc.DownloadStringCompleted += new DownloadStringCompletedEventHandler(async (Object sender, DownloadStringCompletedEventArgs e) => {
                    if (!e.Cancelled && e.Error == null)
                    {
                        this.fetchWeekWeather();
                        Weather w = this.CreateWeatherObject((string)e.Result);
                        this.updateCurrentWeatherUI(w);
                    }
                    else
                    {
                        MessageBox.Show("Invalid city");
                    }
                });

                wc.DownloadStringAsync(new Uri(url));
            }
        }

        private void fetchWeekWeather()
        {
            using (WebClient wc = new WebClient())
            {
                string url = "http://api.openweathermap.org/data/2.5/forecast/daily?q=" + this.city + "&appid=45241fc43340385f1741c0f12a8e2d82&units=metric";
                wc.DownloadStringCompleted += new DownloadStringCompletedEventHandler((Object sender, DownloadStringCompletedEventArgs e) => {
                    if (!e.Cancelled && e.Error == null)
                    {
                        List<Weather> w = this.CreateWeatherObjectArray((string)e.Result);
                        this.updateWeekWeatherUI(w);
                    }
                });

                wc.DownloadStringAsync(new Uri(url));
            }
        }

        private Weather CreateWeatherObject(string result)
        {
            var data = JObject.Parse(result);

            string name = data["name"].ToString() + ", " + data["sys"]["country"].ToString();
            string temp = data["main"]["temp"].ToString() + " °C";
            string weather_main = data["weather"][0]["main"].ToString();
            string icon = data["weather"][0]["icon"].ToString();
            string pressure = data["main"]["pressure"].ToString() + "hPa";
            string humidity = data["main"]["humidity"].ToString() + "%";
            string temp_min = data["main"]["temp_min"].ToString();
            string temp_max = data["main"]["temp_max"].ToString();
            string visibility = ((int)data["visibility"] / 1000) + " km";
            var wind = data["wind"];
            string wind_speed = wind["speed"].ToString() + "km/h";
            string wind_deg = wind["deg"] != null ? wind["deg"].ToString() + "°" : "-";
            DateTime date = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            date.AddSeconds((double)data["dt"]).ToLocalTime();

            return new Weather(name, temp, weather_main, icon, pressure, humidity, temp_min, temp_max, wind_speed, wind_deg, visibility, date);
        }

        private List<Weather> CreateWeatherObjectArray(string result)
        {
            List<Weather> week = new List<Weather>();

            var data = JObject.Parse(result);

            string name = data["city"]["name"].ToString() + ", " + data["city"]["country"].ToString();
            foreach (var day in data["list"])
            {
                System.DateTime date = new System.DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                date = date.AddSeconds((double)day["dt"]);
                string temp = day["temp"]["day"].ToString() + "";
                string weather_main = day["weather"][0]["main"].ToString();
                string icon = day["weather"][0]["icon"].ToString();
                string temp_min = day["temp"]["min"].ToString();
                string temp_max = day["temp"]["max"].ToString();

                Weather weather_day = new Weather(name, "", weather_main, icon, "", "", temp_min, temp_max, "", "", "", date);
                week.Add(weather_day);
            }

            return week;
        }

        private void updateCurrentWeatherUI(Weather weather_now)
        {
            label_place.Text = weather_now.name;
            label_temp.Text = weather_now.temp;
            label_weather.Text = weather_now.weather_main;
            label_min_max.Text = weather_now.temp_min + "/" + weather_now.temp_max + " °C";
            label_pressure.Text = weather_now.pressure;
            label_visibility.Text = weather_now.visibility;
            label_humidity.Text = weather_now.humidity;
            label_wind_speed.Text = weather_now.wind_speed;
            label_wind_deg.Text = weather_now.wind_deg;
            weater_icon_main.Image = weather_now.GetImage();
        }

       private void updateWeekWeatherUI(List<Weather> weather_week)
        {
            for(var i = 1; i < weather_week.Count; i++)
            {
                Label day = (Label)Controls.Find("label_day_" + i, true).FirstOrDefault();
                day.Text = weather_week[i].date.ToString("MM.dd");
                Label min = (Label)Controls.Find("label_min_" + i, true).FirstOrDefault();
                min.Text = weather_week[i].temp_min.ToString() + "°";
                Label max = (Label)Controls.Find("label_max_" + i, true).FirstOrDefault();
                max.Text = weather_week[i].temp_max.ToString() + "°";
                Label weather = (Label)Controls.Find("label_weather_" + i, true).FirstOrDefault();
                weather.Text = weather_week[i].weather_main.ToString();
                PictureBox icon = (PictureBox)Controls.Find("icon_box_" + i, true).FirstOrDefault();
                icon.Image = weather_week[i].GetImage();
            }
        }

        private void HandleOpenSelecCityDialog(string msg)
        {
            CountrySelectForm form = new CountrySelectForm(msg);
            form.city = this.city;

            if (form.ShowDialog(this) == DialogResult.OK)
            {
                this.city = form.city;
                this.FetchCurrentWeather();
            }

            form.Dispose();
        }

        private void Button1_Click_1(object sender, EventArgs e)
        {
            this.HandleOpenSelecCityDialog("Enter city");
        }
    }
}

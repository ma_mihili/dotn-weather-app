﻿namespace weather_app
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel2 = new System.Windows.Forms.Panel();
            this.label_wind_deg = new System.Windows.Forms.Label();
            this.label_wind_speed = new System.Windows.Forms.Label();
            this.label_humidity = new System.Windows.Forms.Label();
            this.label_pressure = new System.Windows.Forms.Label();
            this.label_visibility = new System.Windows.Forms.Label();
            this.label_min_max = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.icon_box_1 = new System.Windows.Forms.PictureBox();
            this.label_weather_1 = new System.Windows.Forms.Label();
            this.label_max_1 = new System.Windows.Forms.Label();
            this.label_min_1 = new System.Windows.Forms.Label();
            this.label_day_1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.icon_box_2 = new System.Windows.Forms.PictureBox();
            this.label_weather_2 = new System.Windows.Forms.Label();
            this.label_max_2 = new System.Windows.Forms.Label();
            this.label_min_2 = new System.Windows.Forms.Label();
            this.label_day_2 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.icon_box_3 = new System.Windows.Forms.PictureBox();
            this.label_weather_3 = new System.Windows.Forms.Label();
            this.label_max_3 = new System.Windows.Forms.Label();
            this.label_min_3 = new System.Windows.Forms.Label();
            this.label_day_3 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.icon_box_4 = new System.Windows.Forms.PictureBox();
            this.label_weather_4 = new System.Windows.Forms.Label();
            this.label_max_4 = new System.Windows.Forms.Label();
            this.label_min_4 = new System.Windows.Forms.Label();
            this.label_day_4 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.icon_box_5 = new System.Windows.Forms.PictureBox();
            this.label_weather_5 = new System.Windows.Forms.Label();
            this.label_max_5 = new System.Windows.Forms.Label();
            this.label_min_5 = new System.Windows.Forms.Label();
            this.label_day_5 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.icon_box_6 = new System.Windows.Forms.PictureBox();
            this.label_weather_6 = new System.Windows.Forms.Label();
            this.label_max_6 = new System.Windows.Forms.Label();
            this.label_min_6 = new System.Windows.Forms.Label();
            this.label_day_6 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.weater_icon_main = new System.Windows.Forms.PictureBox();
            this.label_weather = new System.Windows.Forms.Label();
            this.label_temp = new System.Windows.Forms.Label();
            this.label_place = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icon_box_1)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icon_box_2)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icon_box_3)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icon_box_4)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icon_box_5)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icon_box_6)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.weater_icon_main)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.label_wind_deg);
            this.panel2.Controls.Add(this.label_wind_speed);
            this.panel2.Controls.Add(this.label_humidity);
            this.panel2.Controls.Add(this.label_pressure);
            this.panel2.Controls.Add(this.label_visibility);
            this.panel2.Controls.Add(this.label_min_max);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(12, 144);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(776, 100);
            this.panel2.TabIndex = 5;
            // 
            // label_wind_deg
            // 
            this.label_wind_deg.AutoSize = true;
            this.label_wind_deg.ForeColor = System.Drawing.Color.White;
            this.label_wind_deg.Location = new System.Drawing.Point(525, 56);
            this.label_wind_deg.Name = "label_wind_deg";
            this.label_wind_deg.Size = new System.Drawing.Size(35, 13);
            this.label_wind_deg.TabIndex = 17;
            this.label_wind_deg.Text = "label7";
            // 
            // label_wind_speed
            // 
            this.label_wind_speed.AutoSize = true;
            this.label_wind_speed.ForeColor = System.Drawing.Color.White;
            this.label_wind_speed.Location = new System.Drawing.Point(366, 56);
            this.label_wind_speed.Name = "label_wind_speed";
            this.label_wind_speed.Size = new System.Drawing.Size(35, 13);
            this.label_wind_speed.TabIndex = 16;
            this.label_wind_speed.Text = "label7";
            // 
            // label_humidity
            // 
            this.label_humidity.AutoSize = true;
            this.label_humidity.ForeColor = System.Drawing.Color.White;
            this.label_humidity.Location = new System.Drawing.Point(242, 56);
            this.label_humidity.Name = "label_humidity";
            this.label_humidity.Size = new System.Drawing.Size(35, 13);
            this.label_humidity.TabIndex = 15;
            this.label_humidity.Text = "label7";
            // 
            // label_pressure
            // 
            this.label_pressure.AutoSize = true;
            this.label_pressure.ForeColor = System.Drawing.Color.White;
            this.label_pressure.Location = new System.Drawing.Point(517, 25);
            this.label_pressure.Name = "label_pressure";
            this.label_pressure.Size = new System.Drawing.Size(35, 13);
            this.label_pressure.TabIndex = 14;
            this.label_pressure.Text = "label7";
            // 
            // label_visibility
            // 
            this.label_visibility.AutoSize = true;
            this.label_visibility.ForeColor = System.Drawing.Color.White;
            this.label_visibility.Location = new System.Drawing.Point(378, 25);
            this.label_visibility.Name = "label_visibility";
            this.label_visibility.Size = new System.Drawing.Size(35, 13);
            this.label_visibility.TabIndex = 13;
            this.label_visibility.Text = "label7";
            // 
            // label_min_max
            // 
            this.label_min_max.AutoSize = true;
            this.label_min_max.ForeColor = System.Drawing.Color.White;
            this.label_min_max.Location = new System.Drawing.Point(244, 25);
            this.label_min_max.Name = "label_min_max";
            this.label_min_max.Size = new System.Drawing.Size(35, 13);
            this.label_min_max.TabIndex = 12;
            this.label_min_max.Text = "label7";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(463, 56);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Dew Point";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(189, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Min/Max";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(189, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Humidity";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(463, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Pressure";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(328, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "VIsibility";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(328, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Wind";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.icon_box_1);
            this.panel3.Controls.Add(this.label_weather_1);
            this.panel3.Controls.Add(this.label_max_1);
            this.panel3.Controls.Add(this.label_min_1);
            this.panel3.Controls.Add(this.label_day_1);
            this.panel3.Location = new System.Drawing.Point(12, 250);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(125, 188);
            this.panel3.TabIndex = 18;
            // 
            // icon_box_1
            // 
            this.icon_box_1.Location = new System.Drawing.Point(6, 31);
            this.icon_box_1.Name = "icon_box_1";
            this.icon_box_1.Size = new System.Drawing.Size(48, 48);
            this.icon_box_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.icon_box_1.TabIndex = 4;
            this.icon_box_1.TabStop = false;
            // 
            // label_weather_1
            // 
            this.label_weather_1.AutoSize = true;
            this.label_weather_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_weather_1.ForeColor = System.Drawing.Color.White;
            this.label_weather_1.Location = new System.Drawing.Point(3, 137);
            this.label_weather_1.Name = "label_weather_1";
            this.label_weather_1.Size = new System.Drawing.Size(41, 15);
            this.label_weather_1.TabIndex = 3;
            this.label_weather_1.Text = "label7";
            // 
            // label_max_1
            // 
            this.label_max_1.AutoSize = true;
            this.label_max_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_max_1.ForeColor = System.Drawing.Color.White;
            this.label_max_1.Location = new System.Drawing.Point(3, 108);
            this.label_max_1.Name = "label_max_1";
            this.label_max_1.Size = new System.Drawing.Size(57, 20);
            this.label_max_1.TabIndex = 2;
            this.label_max_1.Text = "label7";
            // 
            // label_min_1
            // 
            this.label_min_1.AutoSize = true;
            this.label_min_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_min_1.ForeColor = System.Drawing.Color.White;
            this.label_min_1.Location = new System.Drawing.Point(66, 111);
            this.label_min_1.Name = "label_min_1";
            this.label_min_1.Size = new System.Drawing.Size(35, 13);
            this.label_min_1.TabIndex = 1;
            this.label_min_1.Text = "label7";
            // 
            // label_day_1
            // 
            this.label_day_1.AutoSize = true;
            this.label_day_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_day_1.ForeColor = System.Drawing.Color.White;
            this.label_day_1.Location = new System.Drawing.Point(0, 0);
            this.label_day_1.Name = "label_day_1";
            this.label_day_1.Size = new System.Drawing.Size(52, 17);
            this.label_day_1.TabIndex = 0;
            this.label_day_1.Text = "label7";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.Controls.Add(this.icon_box_2);
            this.panel4.Controls.Add(this.label_weather_2);
            this.panel4.Controls.Add(this.label_max_2);
            this.panel4.Controls.Add(this.label_min_2);
            this.panel4.Controls.Add(this.label_day_2);
            this.panel4.Location = new System.Drawing.Point(143, 250);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(125, 188);
            this.panel4.TabIndex = 19;
            // 
            // icon_box_2
            // 
            this.icon_box_2.Location = new System.Drawing.Point(6, 31);
            this.icon_box_2.Name = "icon_box_2";
            this.icon_box_2.Size = new System.Drawing.Size(48, 48);
            this.icon_box_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.icon_box_2.TabIndex = 5;
            this.icon_box_2.TabStop = false;
            // 
            // label_weather_2
            // 
            this.label_weather_2.AutoSize = true;
            this.label_weather_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_weather_2.ForeColor = System.Drawing.Color.White;
            this.label_weather_2.Location = new System.Drawing.Point(3, 137);
            this.label_weather_2.Name = "label_weather_2";
            this.label_weather_2.Size = new System.Drawing.Size(41, 15);
            this.label_weather_2.TabIndex = 3;
            this.label_weather_2.Text = "label7";
            // 
            // label_max_2
            // 
            this.label_max_2.AutoSize = true;
            this.label_max_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_max_2.ForeColor = System.Drawing.Color.White;
            this.label_max_2.Location = new System.Drawing.Point(3, 108);
            this.label_max_2.Name = "label_max_2";
            this.label_max_2.Size = new System.Drawing.Size(57, 20);
            this.label_max_2.TabIndex = 2;
            this.label_max_2.Text = "label7";
            // 
            // label_min_2
            // 
            this.label_min_2.AutoSize = true;
            this.label_min_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_min_2.ForeColor = System.Drawing.Color.White;
            this.label_min_2.Location = new System.Drawing.Point(66, 111);
            this.label_min_2.Name = "label_min_2";
            this.label_min_2.Size = new System.Drawing.Size(35, 13);
            this.label_min_2.TabIndex = 1;
            this.label_min_2.Text = "label7";
            // 
            // label_day_2
            // 
            this.label_day_2.AutoSize = true;
            this.label_day_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_day_2.ForeColor = System.Drawing.Color.White;
            this.label_day_2.Location = new System.Drawing.Point(0, 0);
            this.label_day_2.Name = "label_day_2";
            this.label_day_2.Size = new System.Drawing.Size(52, 17);
            this.label_day_2.TabIndex = 0;
            this.label_day_2.Text = "label7";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.panel5.Controls.Add(this.icon_box_3);
            this.panel5.Controls.Add(this.label_weather_3);
            this.panel5.Controls.Add(this.label_max_3);
            this.panel5.Controls.Add(this.label_min_3);
            this.panel5.Controls.Add(this.label_day_3);
            this.panel5.Location = new System.Drawing.Point(274, 250);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(125, 188);
            this.panel5.TabIndex = 19;
            // 
            // icon_box_3
            // 
            this.icon_box_3.Location = new System.Drawing.Point(6, 31);
            this.icon_box_3.Name = "icon_box_3";
            this.icon_box_3.Size = new System.Drawing.Size(48, 48);
            this.icon_box_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.icon_box_3.TabIndex = 6;
            this.icon_box_3.TabStop = false;
            // 
            // label_weather_3
            // 
            this.label_weather_3.AutoSize = true;
            this.label_weather_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_weather_3.ForeColor = System.Drawing.Color.White;
            this.label_weather_3.Location = new System.Drawing.Point(3, 137);
            this.label_weather_3.Name = "label_weather_3";
            this.label_weather_3.Size = new System.Drawing.Size(41, 15);
            this.label_weather_3.TabIndex = 3;
            this.label_weather_3.Text = "label7";
            // 
            // label_max_3
            // 
            this.label_max_3.AutoSize = true;
            this.label_max_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_max_3.ForeColor = System.Drawing.Color.White;
            this.label_max_3.Location = new System.Drawing.Point(3, 108);
            this.label_max_3.Name = "label_max_3";
            this.label_max_3.Size = new System.Drawing.Size(57, 20);
            this.label_max_3.TabIndex = 2;
            this.label_max_3.Text = "label7";
            // 
            // label_min_3
            // 
            this.label_min_3.AutoSize = true;
            this.label_min_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_min_3.ForeColor = System.Drawing.Color.White;
            this.label_min_3.Location = new System.Drawing.Point(66, 111);
            this.label_min_3.Name = "label_min_3";
            this.label_min_3.Size = new System.Drawing.Size(35, 13);
            this.label_min_3.TabIndex = 1;
            this.label_min_3.Text = "label7";
            // 
            // label_day_3
            // 
            this.label_day_3.AutoSize = true;
            this.label_day_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_day_3.ForeColor = System.Drawing.Color.White;
            this.label_day_3.Location = new System.Drawing.Point(0, 0);
            this.label_day_3.Name = "label_day_3";
            this.label_day_3.Size = new System.Drawing.Size(52, 17);
            this.label_day_3.TabIndex = 0;
            this.label_day_3.Text = "label7";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Transparent;
            this.panel6.Controls.Add(this.icon_box_4);
            this.panel6.Controls.Add(this.label_weather_4);
            this.panel6.Controls.Add(this.label_max_4);
            this.panel6.Controls.Add(this.label_min_4);
            this.panel6.Controls.Add(this.label_day_4);
            this.panel6.Location = new System.Drawing.Point(406, 250);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(125, 188);
            this.panel6.TabIndex = 19;
            // 
            // icon_box_4
            // 
            this.icon_box_4.Location = new System.Drawing.Point(6, 31);
            this.icon_box_4.Name = "icon_box_4";
            this.icon_box_4.Size = new System.Drawing.Size(48, 48);
            this.icon_box_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.icon_box_4.TabIndex = 7;
            this.icon_box_4.TabStop = false;
            // 
            // label_weather_4
            // 
            this.label_weather_4.AutoSize = true;
            this.label_weather_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_weather_4.ForeColor = System.Drawing.Color.White;
            this.label_weather_4.Location = new System.Drawing.Point(3, 137);
            this.label_weather_4.Name = "label_weather_4";
            this.label_weather_4.Size = new System.Drawing.Size(41, 15);
            this.label_weather_4.TabIndex = 3;
            this.label_weather_4.Text = "label7";
            // 
            // label_max_4
            // 
            this.label_max_4.AutoSize = true;
            this.label_max_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_max_4.ForeColor = System.Drawing.Color.White;
            this.label_max_4.Location = new System.Drawing.Point(2, 108);
            this.label_max_4.Name = "label_max_4";
            this.label_max_4.Size = new System.Drawing.Size(57, 20);
            this.label_max_4.TabIndex = 2;
            this.label_max_4.Text = "label7";
            // 
            // label_min_4
            // 
            this.label_min_4.AutoSize = true;
            this.label_min_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_min_4.ForeColor = System.Drawing.Color.White;
            this.label_min_4.Location = new System.Drawing.Point(65, 111);
            this.label_min_4.Name = "label_min_4";
            this.label_min_4.Size = new System.Drawing.Size(35, 13);
            this.label_min_4.TabIndex = 1;
            this.label_min_4.Text = "label7";
            // 
            // label_day_4
            // 
            this.label_day_4.AutoSize = true;
            this.label_day_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_day_4.ForeColor = System.Drawing.Color.White;
            this.label_day_4.Location = new System.Drawing.Point(0, 0);
            this.label_day_4.Name = "label_day_4";
            this.label_day_4.Size = new System.Drawing.Size(52, 17);
            this.label_day_4.TabIndex = 0;
            this.label_day_4.Text = "label7";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Transparent;
            this.panel7.Controls.Add(this.icon_box_5);
            this.panel7.Controls.Add(this.label_weather_5);
            this.panel7.Controls.Add(this.label_max_5);
            this.panel7.Controls.Add(this.label_min_5);
            this.panel7.Controls.Add(this.label_day_5);
            this.panel7.Location = new System.Drawing.Point(537, 250);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(125, 188);
            this.panel7.TabIndex = 19;
            // 
            // icon_box_5
            // 
            this.icon_box_5.Location = new System.Drawing.Point(6, 31);
            this.icon_box_5.Name = "icon_box_5";
            this.icon_box_5.Size = new System.Drawing.Size(48, 48);
            this.icon_box_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.icon_box_5.TabIndex = 8;
            this.icon_box_5.TabStop = false;
            // 
            // label_weather_5
            // 
            this.label_weather_5.AutoSize = true;
            this.label_weather_5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_weather_5.ForeColor = System.Drawing.Color.White;
            this.label_weather_5.Location = new System.Drawing.Point(3, 137);
            this.label_weather_5.Name = "label_weather_5";
            this.label_weather_5.Size = new System.Drawing.Size(41, 15);
            this.label_weather_5.TabIndex = 3;
            this.label_weather_5.Text = "label7";
            // 
            // label_max_5
            // 
            this.label_max_5.AutoSize = true;
            this.label_max_5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_max_5.ForeColor = System.Drawing.Color.White;
            this.label_max_5.Location = new System.Drawing.Point(3, 108);
            this.label_max_5.Name = "label_max_5";
            this.label_max_5.Size = new System.Drawing.Size(57, 20);
            this.label_max_5.TabIndex = 2;
            this.label_max_5.Text = "label7";
            // 
            // label_min_5
            // 
            this.label_min_5.AutoSize = true;
            this.label_min_5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_min_5.ForeColor = System.Drawing.Color.White;
            this.label_min_5.Location = new System.Drawing.Point(66, 111);
            this.label_min_5.Name = "label_min_5";
            this.label_min_5.Size = new System.Drawing.Size(35, 13);
            this.label_min_5.TabIndex = 1;
            this.label_min_5.Text = "label7";
            // 
            // label_day_5
            // 
            this.label_day_5.AutoSize = true;
            this.label_day_5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_day_5.ForeColor = System.Drawing.Color.White;
            this.label_day_5.Location = new System.Drawing.Point(0, 0);
            this.label_day_5.Name = "label_day_5";
            this.label_day_5.Size = new System.Drawing.Size(52, 17);
            this.label_day_5.TabIndex = 0;
            this.label_day_5.Text = "label7";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Transparent;
            this.panel8.Controls.Add(this.icon_box_6);
            this.panel8.Controls.Add(this.label_weather_6);
            this.panel8.Controls.Add(this.label_max_6);
            this.panel8.Controls.Add(this.label_min_6);
            this.panel8.Controls.Add(this.label_day_6);
            this.panel8.Location = new System.Drawing.Point(668, 250);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(125, 188);
            this.panel8.TabIndex = 19;
            // 
            // icon_box_6
            // 
            this.icon_box_6.Location = new System.Drawing.Point(6, 31);
            this.icon_box_6.Name = "icon_box_6";
            this.icon_box_6.Size = new System.Drawing.Size(48, 48);
            this.icon_box_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.icon_box_6.TabIndex = 9;
            this.icon_box_6.TabStop = false;
            // 
            // label_weather_6
            // 
            this.label_weather_6.AutoSize = true;
            this.label_weather_6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_weather_6.ForeColor = System.Drawing.Color.White;
            this.label_weather_6.Location = new System.Drawing.Point(3, 137);
            this.label_weather_6.Name = "label_weather_6";
            this.label_weather_6.Size = new System.Drawing.Size(41, 15);
            this.label_weather_6.TabIndex = 3;
            this.label_weather_6.Text = "label7";
            // 
            // label_max_6
            // 
            this.label_max_6.AutoSize = true;
            this.label_max_6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_max_6.ForeColor = System.Drawing.Color.White;
            this.label_max_6.Location = new System.Drawing.Point(3, 108);
            this.label_max_6.Name = "label_max_6";
            this.label_max_6.Size = new System.Drawing.Size(57, 20);
            this.label_max_6.TabIndex = 2;
            this.label_max_6.Text = "label7";
            // 
            // label_min_6
            // 
            this.label_min_6.AutoSize = true;
            this.label_min_6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_min_6.ForeColor = System.Drawing.Color.White;
            this.label_min_6.Location = new System.Drawing.Point(66, 111);
            this.label_min_6.Name = "label_min_6";
            this.label_min_6.Size = new System.Drawing.Size(35, 13);
            this.label_min_6.TabIndex = 1;
            this.label_min_6.Text = "label7";
            // 
            // label_day_6
            // 
            this.label_day_6.AutoSize = true;
            this.label_day_6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_day_6.ForeColor = System.Drawing.Color.White;
            this.label_day_6.Location = new System.Drawing.Point(0, 0);
            this.label_day_6.Name = "label_day_6";
            this.label_day_6.Size = new System.Drawing.Size(52, 17);
            this.label_day_6.TabIndex = 0;
            this.label_day_6.Text = "label7";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.weater_icon_main);
            this.panel1.Controls.Add(this.label_weather);
            this.panel1.Controls.Add(this.label_temp);
            this.panel1.Controls.Add(this.label_place);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(776, 126);
            this.panel1.TabIndex = 20;
            // 
            // weater_icon_main
            // 
            this.weater_icon_main.Location = new System.Drawing.Point(226, 17);
            this.weater_icon_main.Name = "weater_icon_main";
            this.weater_icon_main.Size = new System.Drawing.Size(96, 96);
            this.weater_icon_main.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.weater_icon_main.TabIndex = 3;
            this.weater_icon_main.TabStop = false;
            // 
            // label_weather
            // 
            this.label_weather.AutoSize = true;
            this.label_weather.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_weather.ForeColor = System.Drawing.Color.White;
            this.label_weather.Location = new System.Drawing.Point(326, 84);
            this.label_weather.Name = "label_weather";
            this.label_weather.Size = new System.Drawing.Size(79, 29);
            this.label_weather.TabIndex = 2;
            this.label_weather.Text = "label7";
            // 
            // label_temp
            // 
            this.label_temp.AutoSize = true;
            this.label_temp.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_temp.ForeColor = System.Drawing.Color.White;
            this.label_temp.Location = new System.Drawing.Point(324, 47);
            this.label_temp.Name = "label_temp";
            this.label_temp.Size = new System.Drawing.Size(102, 37);
            this.label_temp.TabIndex = 1;
            this.label_temp.Text = "label7";
            // 
            // label_place
            // 
            this.label_place.AutoSize = true;
            this.label_place.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_place.ForeColor = System.Drawing.Color.White;
            this.label_place.Location = new System.Drawing.Point(325, 16);
            this.label_place.Name = "label_place";
            this.label_place.Size = new System.Drawing.Size(86, 31);
            this.label_place.TabIndex = 0;
            this.label_place.Text = "label7";
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(728, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(48, 48);
            this.button1.TabIndex = 4;
            this.button1.Text = "+";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::weather_app.Properties.Resources.WeatherLock_Lock_screen_730x480;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Weather App";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icon_box_1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icon_box_2)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icon_box_3)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icon_box_4)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icon_box_5)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.icon_box_6)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.weater_icon_main)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label_visibility;
        private System.Windows.Forms.Label label_min_max;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label_wind_deg;
        private System.Windows.Forms.Label label_wind_speed;
        private System.Windows.Forms.Label label_humidity;
        private System.Windows.Forms.Label label_pressure;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label_weather_1;
        private System.Windows.Forms.Label label_max_1;
        private System.Windows.Forms.Label label_min_1;
        private System.Windows.Forms.Label label_day_1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label_weather_2;
        private System.Windows.Forms.Label label_max_2;
        private System.Windows.Forms.Label label_min_2;
        private System.Windows.Forms.Label label_day_2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label_weather_3;
        private System.Windows.Forms.Label label_max_3;
        private System.Windows.Forms.Label label_min_3;
        private System.Windows.Forms.Label label_day_3;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label_weather_4;
        private System.Windows.Forms.Label label_max_4;
        private System.Windows.Forms.Label label_min_4;
        private System.Windows.Forms.Label label_day_4;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label_weather_5;
        private System.Windows.Forms.Label label_max_5;
        private System.Windows.Forms.Label label_min_5;
        private System.Windows.Forms.Label label_day_5;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label_weather_6;
        private System.Windows.Forms.Label label_max_6;
        private System.Windows.Forms.Label label_min_6;
        private System.Windows.Forms.Label label_day_6;
        private System.Windows.Forms.PictureBox icon_box_1;
        private System.Windows.Forms.PictureBox icon_box_2;
        private System.Windows.Forms.PictureBox icon_box_3;
        private System.Windows.Forms.PictureBox icon_box_4;
        private System.Windows.Forms.PictureBox icon_box_5;
        private System.Windows.Forms.PictureBox icon_box_6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox weater_icon_main;
        private System.Windows.Forms.Label label_weather;
        private System.Windows.Forms.Label label_temp;
        private System.Windows.Forms.Label label_place;
        private System.Windows.Forms.Button button1;
    }
}


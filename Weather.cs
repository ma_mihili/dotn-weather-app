﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace weather_app
{
    class Weather
    {
        public string name;
        public string temp;
        public string weather_main;
        public string icon;
        public string pressure;
        public string visibility;
        public string humidity;
        public string temp_min;
        public string temp_max;
        public string wind_speed;
        public string wind_deg;
        public DateTime date;
        public string updated_at;

        public Weather(string name, string temp, string weather, string icon, string pressure, string humidity, string temp_min, string temp_max, string wind_speed, string wind_deg, string visibility, DateTime date) 
        {
            this.name = name;
            this.temp = temp;
            this.weather_main = weather;
            this.icon = icon;
            this.pressure = pressure;
            this.humidity = humidity;
            this.temp_max = temp_max;
            this.temp_min = temp_min;
            this.wind_speed = wind_speed;
            this.wind_deg = wind_deg;
            this.visibility = visibility;
            this.date = date;
            this.updated_at = DateTime.UtcNow.Date.ToString("dd/MM/yyyy hh:mm");
        }

        public System.Drawing.Bitmap GetImage()
        {
            switch (this.icon)
            {
                case "02d":
                    return Properties.Resources.iconfinder_weather_2_2682849;
                case "03d":
                case "04d":
                    return Properties.Resources.iconfinder_weather_1_2682850;
                case "09d":
                case "10d":
                    return Properties.Resources.iconfinder_weather_16_2682835;
                case "11d":
                    return Properties.Resources.iconfinder_weather_23_2682828;
                case "13d":
                    return Properties.Resources.iconfinder_weather_35_2682816;
                case "50d":
                    return Properties.Resources.iconfinder_weather_38_2682813;
                case "01d":
                default:
                    return Properties.Resources.iconfinder_weather_3_2682848;
            }
        }
    }
}
